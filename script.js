let dropBtn = document.querySelector('.bars-icon');

let menu = document.querySelector('.dropdown-content');

dropBtn.addEventListener('click', function () {
    dropBtn.classList.toggle('fa-times');
    if (dropBtn.classList.contains('fa-times')) {
        menu.style.display = 'block';
    } else {
        menu.style.display = 'none';
    }
});



